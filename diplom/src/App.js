import './App.css';
import { Routes, Route, } from "react-router-dom";
import { Menu } from "./pages/Menu/Menu";
import { Basket } from "./pages/Basket/Basket";

function App() {
  return (
      <div className="App">
        <Routes>
          <Route path="/" element={<Menu />} />
          <Route path="/basket" element={<Basket />} />
        </Routes>
      </div>
  );
}
export default App;
